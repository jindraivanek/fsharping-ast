module M

module N =
    let foo = 
        let goo = 42
        let goo2 = goo |> fun x -> x + 1
        match Some 1 with
        | Some num -> 
            if true then 
                let deep = goo2
                deep
            else num
        | None -> goo