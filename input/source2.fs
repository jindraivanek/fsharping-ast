let add3 x y z =
    let a = x + y
    a + z
add3 10 30 2 |> printfn "%i"