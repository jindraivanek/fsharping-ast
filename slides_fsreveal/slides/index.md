- title : Ast
- description : Ast
- author : Jindřich Ivánek
- theme : simple
- transition : default

***

## Procházka po F# AST stromě

<img src="images/tree_final.gif" style="background: transparent; border-style: none;" width=90% />

Jindřich Ivánek - [@jindraivanek](http://www.twitter.com/jindraivanek)


***

### AST

* Datová reprezentace kódu
* Strom odpovídá gramatice podle specifikace
* Závorky, priorita operátorů reprezentováno pozicí ve stromě
* Jazykové konstrukce (`if-then-else`, `let`, `match`, ...) -> speciální uzly stromu

---

## F# Compiler services

 <img src="images/fscomp-phases.png" style="background: transparent; border-style: none;" width=75% />

---

### F# AST - ukázka 1

```printfn "Hello Fsharping!"```

---

### F# AST - ukázka 1

```printfn "Hello Fsharping!"```

<img src="images/ast1.svg" style="background: transparent; border-style: none;"  width=100% />

---

### F# AST - ukázka 2

```
let add x y =
    x + y
add 40 2 |> printfn "%i"
```

---

### F# AST - ukázka 2

<img src="images/ast2.svg" style="background: transparent; border-style: none;"  width=100% />

---

### F# AST - ukázka 3

```
let add3 x y z =
    let a = x + y
    a + z
add3 10 30 2 |> printfn "%i"
```

---

<img src="images/ast3.svg" style="background: transparent; border-style: none;"  width=100% />

***


### Použití F# (untyped) AST

* statická analýza kódu
* formátování kódu
* transpilace/kompilace do jiného jazyka

---

### Použití F# (untyped) AST

* Fable
* F#.Formatting
* Fantomas
* FSharpLint
* Fez
* Mechanic
* ...

***

### Livecoding

Cíl: Nalezení bindingu s největší hloubkou

Zde binding = jakýkoli pojmenovaný identifikátor -- `let`, lambda funkce, `match` case

Jedna úroveň hloubky - `module`, `let`, `fun`, `match` case, `then`, `else`

---

###Livecoding 1

* Výpis AST

---

###Livecoding 2

* Bindings 1. úrovně

---

###Livecoding 3

* Výpis hloubky
* `NestedModule`

---

<img src="images/deeper.jpg" style="background: transparent; border-style: none;"  width=100% />

---

###Livecoding 4

* Vnitřní `let` bindings - `Expr`

---

###Livecoding 5

* lambda funkce
* `match`
* `if-then-else`
* seřazení výstupu
* průchod adresáře

*** 

### Odkazy

* https://fsharp.github.io/FSharp.Compiler.Service/untypedtree.html
* https://fsharp.github.io/2015/09/29/fsharp-compiler-guide.html

intro animation source: https://www.youtube.com/watch?v=H7nezRHosB0

*** 

# Projekt Mechanic

<img src="images/mechanic-logo-horizontal.png" style="background: transparent; border-style: none;"  width=100% />

---

### Projekt Mechanic

<img src="images/first-tweet.png" style="background: transparent; border-style: none;"  width=100% />

---

### Projekt Mechanic

* Automatické seřazení zdrojových F# souborů
* Pomocí analýzy AST nalezení závislostí mezi soubory
* Nové pořadí co nejblíže původnímu pořadí

---

### Projekt Mechanic

<img src="images/contributors.png" style="background: transparent; border-style: none;"  width=100% />

---

## VScode plugin

* "mechanic" v Marketplace
* https://github.com/ionide/ionide-vscode-mechanic
* Hlavní autor: @MangelMaxime

***

## Topological sort

* Orientovaný graf, vrcholy = soubory, hrany = závislosti
* Topologické uspořádání - očíslování vrcholů *p* tž. pro každou orientovanou cestu *A -> B* platí: *p(A) < p(B)*
* Graf je buď acyklický (DAG) a existuje topologické uspořádání, nebo má cyklus -> circular dependency

---

### Kahnův algoritmus

<img src="images/Directed_acyclic_graph_2.svg.png" style="background: transparent; border-style: none;"  width=50% />

Topologické uspořádání např.:
    5, 7, 3, 11, 8, 2, 9, 
    
---

### Kahnův algoritmus

* vrcholy do kterých nevede hrana odebereme a přidáme do výstupního pořadí
* opakujeme dokud máme nějaké vrcholy

---

### Kahnův algoritmus - minimální počet operací

* jedna operace = přehození sousedních prvků
* problém: nalezení pořadí které se liší od původního minimálním množství operací
* současné řešení (aproximace): vrcholy odebíráme po jednom, z možných vrcholů vybere ten který je v origináním pořadí nejvýše

***

## Nalezení závislostí

* deklarace symbolů (funkce, typy, union cases, record fields) - včetně namespaců
* použití symbolů
* `open` deklarace
->
* spárování symbolů s jejich definicemi - vyzkoušení všech variant z `open`

---

### Nalezení závislostí - namespace merge
```
module Test1
  module M =
    let x = 42
```

```
module Test2
  open Test1.M
  let y = M.x
```

`open Test1.M + M.x` -> `Test1.M.x`

`open A.B.C + C.D` -> `A.B.C.D`

---

### Vytažení symbolů z AST
* to vypadá na spoustu práce
* naštěstí něco takového už existuje - `AstTraverse`

---

<img src="images/easy.gif" style="background: transparent; border-style: none;"  width=100% />

---

<img src="images/Ieo6ddj.gif" style="background: transparent; border-style: none;"  width=100% />

***

### `open` order

```
//A.fs
module Test1
let x = 1

//B.fs
module Test2
let x = 1

//C.fs
module Test3
module M1 =
    open Test1
    open Test2
    let y = x
```

závislosti: B -> C

---

### `open` scope

```
//A.fs
module Test1
  let x = 1

//B.fs
module Test2
  let x = 1

//C.fs
module Test3
  open Test1
  module M1 =
      open Test2
  module M2 =
      let y = x
```

závislosti: B -> C

---

### `namespace`

```
//A.fs
namespace Test
  module M =
      let x = 42

//B.fs
namespace Test
  module M2 =
      let y = M.x
```

závislosti: A -> B

---

### Shadowing

```
//A.fs
module N.M1
let x = 42

//B.fs
module N.M2
let x = 43
let y = x
```

závislosti: nic

---

### Shadowing 2

```
//A.fs
namespace N
let x = 42

//B.fs
namespace N
let f x =
    let y = x
    y
```

závislosti: nic

---

### Shadowing 3

```
//A.fs
namespace N
let x = 42

//B.fs
namespace N
let y = 
    let x = 42
    x
```

závislosti: nic

---

### Shadowing 4

```
//A.fs
namespace N
let x = 42

//B.fs
namespace N
  let y = 
      let w = x
      let x = 42
      x
```

závislosti: A -> B

---

### `[<AutoOpen>]`

```
//A.fs
[<AutoOpen>]
module M
let x = 42

//B.fs
let y = x
```

závislosti: A -> B

---

### `[<AutoOpen>] 2`

```
//A.fs
[<AutoOpen>]
module M
let x = 42

//B.fs
module M2
let x = "hello"

//C.fs
open M2
let y = x
```

závislosti: B -> C

---

### Externí závislosti

```
//A.fs
module M
let x = System.IO.DirectoryInfo(".")

//B.fs
[<AutoOpen>]
module M2
let System.IO.DirectoryInfo x = ()
```

závislosti: nic

***

### Projekt Mechanic

<img src="images/mechanic-logo-horizontal.png" style="background: transparent; border-style: none;"  width=80% />

* https://github.com/fsprojects/Mechanic

---

### Odkazy

* https://github.com/fsprojects/Mechanic
* https://github.com/ionide/ionide-vscode-mechanic
* https://github.com/fsprojects/Mechanic/blob/master/OrderingAlg.md