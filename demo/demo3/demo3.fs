module demo1

open Microsoft.FSharp.Compiler.SourceCodeServices
open Microsoft.FSharp.Compiler.Ast

type AstContext = { depth: int }
let inc ctx = {ctx with depth=ctx.depth+1}

let checker = FSharpChecker.Create()

let visitLongIdent ctx label (longId: LongIdent) = 
    [longId |> List.map (fun lId -> lId.idText) |> String.concat "." |> sprintf "%02i: %s %s" ctx.depth label]

let rec visitPattern ctx = function
    | SynPat.LongIdent(LongIdentWithDots(lIds,_),_,_,_,_,_) -> visitLongIdent ctx "let" lIds
    | SynPat.Named(pat, ident, _, _, _) ->
        visitLongIdent ctx "let" [ident]
        @ visitPattern ctx pat
    | _ -> []

let visitBinding ctx = function
    | SynBinding.Binding (headPat=pat; expr=expr) -> visitPattern ctx pat

let rec visitDecl ctx = function
    | SynModuleDecl.Let (_,bindings,_) -> bindings |> List.collect (visitBinding ctx)
    | SynModuleDecl.NestedModule (ComponentInfo(longId=longId),_,decls,_,_) ->
        visitLongIdent ctx "module" longId
        @ (decls |> List.collect (visitDecl <| inc ctx))
    | _ -> []

let visitModule ctx = function
    | SynModuleOrNamespace (longId=longId; decls=decls) -> 
        visitLongIdent ctx "module" longId
        @ (decls |> List.collect (visitDecl <| inc ctx))

let visitTree ctx = function
    | ParsedInput.ImplFile (ParsedImplFileInput (modules=modules)) -> 
        modules |> List.collect (visitModule ctx)
    | _ -> []

[<EntryPoint>]
let main argv =
    let file = argv.[0]
    let source = System.IO.File.ReadAllText file
    let (parseOpts,_) = checker.GetParsingOptionsFromCommandLineArgs [file]
    let result = checker.ParseFile(file, source, parseOpts) |> Async.RunSynchronously
    let (Some tree) = result.ParseTree
    printfn "%A" tree
    printfn "----"
    visitTree {depth=0} tree |> Seq.iter (printfn "%s")

    0 // return an integer exit code
