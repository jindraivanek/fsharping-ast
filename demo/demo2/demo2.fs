module demo1

open Microsoft.FSharp.Compiler.SourceCodeServices
open Microsoft.FSharp.Compiler.Ast

let checker = FSharpChecker.Create()

let visitLongIdent label (longId: LongIdent) = 
    [longId |> List.map (fun lId -> lId.idText) |> String.concat "." |> sprintf "%s: %s" label]

let rec visitPattern = function
    | SynPat.LongIdent(LongIdentWithDots(lIds,_),_,_,_,_,_) -> visitLongIdent "let" lIds
    | SynPat.Named(pat, ident, _, _, _) ->
        visitLongIdent "let" [ident] @ visitPattern pat
    | _ -> []

let visitBinding = function
    | SynBinding.Binding (headPat=pat; expr=expr) -> visitPattern pat

let rec visitDecl = function
    | SynModuleDecl.Let (_,bindings,_) -> bindings |> List.collect visitBinding
    | _ -> []

let visitModule = function
    | SynModuleOrNamespace (longId=longId; decls=decls) -> 
        visitLongIdent "module" longId
        @ (decls |> List.collect visitDecl)

let visitTree = function
    | ParsedInput.ImplFile (ParsedImplFileInput (modules=modules)) -> modules |> List.collect visitModule
    | _ -> []

[<EntryPoint>]
let main argv =
    let file = argv.[0]
    let source = System.IO.File.ReadAllText file
    let (parseOpts,_) = checker.GetParsingOptionsFromCommandLineArgs [file]
    let result = checker.ParseFile(file, source, parseOpts) |> Async.RunSynchronously
    let (Some tree) = result.ParseTree
    printfn "%A" tree
    printfn "----"
    visitTree tree |> Seq.iter (printfn "%s")

    0 // return an integer exit code
