module demo1

open Microsoft.FSharp.Compiler.SourceCodeServices

let checker = FSharpChecker.Create()

[<EntryPoint>]
let main argv =
    let file = argv.[0]
    let source = System.IO.File.ReadAllText file
    let (parseOpts,_) = checker.GetParsingOptionsFromCommandLineArgs [file]
    let result = checker.ParseFile(file, source, parseOpts) |> Async.RunSynchronously
    let (Some tree) = result.ParseTree
    printfn "%A" tree
    0 // return an integer exit code
