module demo1

open Microsoft.FSharp.Compiler.SourceCodeServices
open Microsoft.FSharp.Compiler.Ast

type AstContext = { depth: int }
let inc ctx = {ctx with depth=ctx.depth+1}

let checker = FSharpChecker.Create()

let visitLongIdent ctx label (longId: LongIdent) = 
    match longId with
    | [] -> []
    | _ ->
    let hd = List.head longId
    longId |> List.map (fun lId -> lId.idText) |> String.concat "." 
    |> fun x -> [sprintf "%02i %s: %s %s:%i" ctx.depth label x hd.idRange.FileName hd.idRange.StartLine]

let rec visitPattern ctx label = function
    | SynPat.LongIdent(LongIdentWithDots(lIds,_),_,_,args,_,_) -> 
        match args with
        | SynConstructorArgs.Pats [] 
        | SynConstructorArgs.NamePatPairs ([],_) -> visitLongIdent ctx label lIds
        | SynConstructorArgs.Pats pats -> pats |> List.collect (visitPattern ctx label)
        | SynConstructorArgs.NamePatPairs (namedPairs, _) -> 
            namedPairs |> List.collect (fun (ident, pat) -> 
                visitLongIdent ctx "named" [ident] @ visitPattern ctx label pat)
    | SynPat.Named(pat, ident, _, _, _) ->
        visitLongIdent ctx label [ident]
        @ visitPattern ctx label pat
    | _ -> []

let rec visitSimplePat ctx = function
    | SynSimplePat.Id(ident=ident) -> visitLongIdent ctx "lambda" [ident]
    | SynSimplePat.Typed(pat, _, _) -> visitSimplePat ctx pat
    | SynSimplePat.Attrib _ -> []

let rec visitSimplePats ctx = function
    | SynSimplePats.SimplePats(pats,_) -> pats |> List.collect (visitSimplePat ctx)
    | SynSimplePats.Typed(pats, _, _) -> visitSimplePats ctx pats

let rec visitMatchClause ctx = function
    | SynMatchClause.Clause (pat, _, expr, _, _) ->
        visitPattern ctx "match" pat
        @ visitExpr ctx expr

and visitExpr ctx = function
    | SynExpr.LetOrUse(bindings=bindings; body=expr) ->
        (bindings |> List.collect (visitBinding ctx))
        @ visitExpr ctx expr
    | SynExpr.Lambda(args=args; body=body) ->
        visitSimplePats (inc ctx) args
        @ visitExpr (inc ctx) body
    | SynExpr.Match (clauses=clauses) -> clauses |> List.collect (visitMatchClause (inc ctx))
    | SynExpr.IfThenElse (thenExpr=thenExpr; elseExpr=elseExpr) ->
        visitExpr (inc ctx) thenExpr
        @ (elseExpr |> Option.map (
            function 
            | SynExpr.IfThenElse _ as x -> visitExpr ctx x 
            | x -> visitExpr (inc ctx) x) 
          |> Option.defaultValue [])
    | SynExpr.Typed (expr,_,_) -> visitExpr ctx expr
    | _ -> []

and visitBinding ctx = function
    | SynBinding.Binding (headPat=pat; expr=expr) -> 
        visitPattern ctx "let" pat
        @ visitExpr (inc ctx) expr

let rec visitDecl ctx = function
    | SynModuleDecl.Let (_,bindings,_) -> bindings |> List.collect (visitBinding ctx)
    | SynModuleDecl.NestedModule (ComponentInfo(longId=longId),_,decls,_,_) ->
        visitLongIdent ctx "module" longId
        @ (decls |> List.collect (visitDecl <| inc ctx))
    | _ -> []

let visitModule ctx = function
    | SynModuleOrNamespace (longId=longId; decls=decls) -> 
        visitLongIdent ctx "module" longId
        @ (decls |> List.collect (visitDecl <| inc ctx))

let visitTree ctx = function
    | ParsedInput.ImplFile (ParsedImplFileInput (modules=modules)) -> 
        modules |> List.collect (visitModule ctx)
    | _ -> []

let run file =
    let source = System.IO.File.ReadAllText file
    let (parseOpts,_) = checker.GetParsingOptionsFromCommandLineArgs [file]
    let result = checker.ParseFile(file, source, parseOpts) |> Async.RunSynchronously
    let (Some tree) = result.ParseTree
    //printfn "%A" tree
    //printfn "----"
    visitTree {depth=0} tree

let rec getFiles dir =
    Seq.append
        (System.IO.Directory.EnumerateFiles dir)
        (System.IO.Directory.EnumerateDirectories dir |> Seq.collect getFiles)

[<EntryPoint>]
let main argv =
    let dir = argv.[0]
    getFiles dir |> Seq.filter (fun f -> f.EndsWith ".fs") |> Seq.collect run
    |> Seq.sortBy id |> Seq.iter (printfn "%s")

    0 // return an integer exit code
